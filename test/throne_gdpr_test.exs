defmodule ThroneGDPRTest do
  use ExUnit.Case
  alias ThroneGDPR.MatchData

  doctest ThroneGDPR

  test "gets the matches from a GDPR JSON file" do
    {json, matches} = ThroneGDPR.Generator.GDPRFile.generate()

    expected_matches = sort_matches(matches)
    actual_matches = sort_matches(ThroneGDPR.get_matches(json))

    assert expected_matches === actual_matches
  end

  # .......................................................................... #
  # Helper functions

  defp sort_matches(matches) do
    Enum.sort(matches, &sort_by_index/2)
  end

  defp sort_by_index(%MatchData{index: index_1}, %MatchData{index: index_2}) do
    index_1 <= index_2
  end
end
