defmodule ThroneGDPR.Generator.MatchData do
  require ExUnitProperties

  def generate do
    ExUnitProperties.gen all(
      index <- StreamData.integer(),
      user_id <- StreamData.string(:alphanumeric),
      user_name <- StreamData.string(:alphanumeric),
      player_name <- StreamData.string(:alphanumeric),
      player_index <- StreamData.integer(-1..15),
      start_class <- StreamData.string(:alphanumeric),
      start_faction <- StreamData.string(:alphanumeric),
      end_class <- StreamData.string(:alphanumeric),
      end_faction <- StreamData.string(:alphanumeric),
      last_journal_left <- StreamData.string(:alphanumeric),
      last_journal_right <- StreamData.string(:alphanumeric),
      loaded_in_game? <- StreamData.boolean(),
      alive? <- StreamData.boolean(),
      won? <- StreamData.boolean(),
      connected_until_end? <- StreamData.boolean(),
      eligible_to_reconnect? <- StreamData.boolean(),
      left_game_reason <- StreamData.string(:alphanumeric),
      p_con <- StreamData.integer()
    ) do
      %ThroneGDPR.MatchData{
        index: index,
        user_id: user_id,
        user_name: user_name,
        player_name: player_name,
        player_index: player_index,
        start_class: start_class,
        start_faction: start_faction,
        end_class: end_class,
        end_faction: end_faction,
        last_journal_left: last_journal_left,
        last_journal_right: last_journal_right,
        loaded_in_game?: loaded_in_game?,
        alive?: alive?,
        won?: won?,
        connected_until_end?: connected_until_end?,
        eligible_to_reconnect?: eligible_to_reconnect?,
        left_game_reason: left_game_reason,
        p_con: p_con
      }
    end
  end

  def to_map(%ThroneGDPR.MatchData{} = match_data) do
    {"matchPlayer#{match_data.index}",
     %{
       "pid" => match_data.user_id,
       "dName" => match_data.user_name,
       "ign" => match_data.player_name,
       "piIndex" => match_data.player_index,
       "startClass" => match_data.start_class,
       "endClass" => match_data.end_class,
       "startFaction" => match_data.start_faction,
       "endFaction" => match_data.end_faction,
       "isAlive" => match_data.alive?,
       "loadedInGame" => match_data.loaded_in_game?,
       "wasConnectedUntilEnd" => match_data.connected_until_end?,
       "leftGameReason" => match_data.left_game_reason,
       "isEligibleToReconnect" => match_data.eligible_to_reconnect?,
       "pConInt" => match_data.p_con,
       "won" => match_data.won?,
       "lastJournalLeft" => match_data.last_journal_left,
       "lastJournalRight" => match_data.last_journal_right
     }}
  end
end
