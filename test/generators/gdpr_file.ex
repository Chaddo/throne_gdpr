defmodule ThroneGDPR.Generator.GDPRFile do
  require ExUnitProperties

  alias ThroneGDPR.MatchData, as: MatchData
  alias ThroneGDPR.Generator.MatchData, as: MatchDataGenerator

  def generate do
    # The amount of MatchData entries to generate for the GDPR file
    iterations = Enum.random(1..20)

    matches =
      MatchDataGenerator.generate()
      |> Enum.take(iterations)
      |> Enum.uniq_by(&unique_index/1)
      |> Enum.sort(&sort_by_index/2)

    json =
      matches
      |> Enum.reduce(%{}, &reduce_match/2)
      |> insert_json_base
      |> Jason.encode!()

    {json, matches}
  end

  # .......................................................................... #
  # Helper functions

  defp unique_index(%MatchData{index: index}), do: index

  defp reduce_match(match_data, acc) do
    {key, value} = MatchDataGenerator.to_map(match_data)

    Map.put(acc, key, value)
  end

  defp sort_by_index(%MatchData{index: index_1}, %MatchData{index: index_2}) do
    index_1 <= index_2
  end

  defp insert_json_base(map), do: %{"scriptData" => %{"masterDataObj" => map}}
end
