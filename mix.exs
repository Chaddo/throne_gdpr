defmodule ThroneGDPR.MixProject do
  use Mix.Project

  def project do
    [
      app: :throne_gdpr,
      version: "0.1.0",
      elixir: "~> 1.9-rc",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  defp elixirc_paths(:test),
    do: ["lib", "test"]

  defp elixirc_paths(_),
    do: ["lib"]

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1"},
      {:stream_data, "~> 0.4", only: [:dev, :test]}
    ]
  end
end
