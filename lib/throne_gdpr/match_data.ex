defmodule ThroneGDPR.MatchData do
  @moduledoc """
  Struct describing the metadata of a ToL match.
  """

  alias __MODULE__

  defstruct [
    :index,
    :user_id,
    :user_name,
    :player_name,
    :player_index,
    :start_class,
    :start_faction,
    :end_class,
    :end_faction,
    :last_journal_left,
    :last_journal_right,
    :loaded_in_game?,
    :alive?,
    :won?,
    :connected_until_end?,
    :eligible_to_reconnect?,
    :left_game_reason,
    :p_con
  ]

  @typedoc "ToL match metadata"
  @type t :: %MatchData{
          index: integer,
          user_id: String.t(),
          user_name: String.t(),
          player_name: String.t(),
          player_index: integer,
          start_class: String.t(),
          start_faction: String.t(),
          end_class: String.t(),
          end_faction: String.t(),
          last_journal_left: String.t(),
          last_journal_right: String.t(),
          loaded_in_game?: boolean,
          alive?: boolean,
          won?: boolean,
          connected_until_end?: boolean,
          eligible_to_reconnect?: boolean,
          left_game_reason: String.t(),
          p_con: integer
        }
end
