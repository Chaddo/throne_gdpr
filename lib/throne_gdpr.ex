defmodule ThroneGDPR do
  alias ThroneGDPR.MatchData

  def get_matches(binary) do
    binary
    |> Jason.decode!()
    |> Map.get("scriptData")
    |> Map.get("masterDataObj")
    |> Enum.reduce([], &to_match/2)
  end

  # .......................................................................... #
  # Helper functions

  # Transform an input map to a collection of matches
  defp to_match({"matchPlayer" <> index, match_data}, accumulator) do
    match = %MatchData{
      index: String.to_integer(index),
      user_id: match_data["pid"],
      user_name: match_data["dName"],
      player_name: match_data["ign"],
      player_index: match_data["piIndex"],
      start_class: match_data["startClass"],
      start_faction: match_data["startFaction"],
      end_class: match_data["endClass"],
      end_faction: match_data["endFaction"],
      last_journal_left: match_data["lastJournalLeft"],
      last_journal_right: match_data["lastJournalRight"],
      loaded_in_game?: match_data["loadedInGame"],
      alive?: match_data["isAlive"],
      won?: match_data["won"],
      connected_until_end?: match_data["wasConnectedUntilEnd"],
      eligible_to_reconnect?: match_data["isEligibleToReconnect"],
      left_game_reason: match_data["leftGameReason"],
      p_con: match_data["pConInt"]
    }

    [match | accumulator]
  end

  defp to_match(_, accumulator), do: accumulator
end
