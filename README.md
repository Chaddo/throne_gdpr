# Throne GDPR Log Parser

This is a sample project to get used to the features of Elixir.

## Usage

```elixir
"path/to/file.txt"
|> File.read!
# |> strip_bom # The parser doesn't handle BOMs in the current state
|> ThroneGDPR.get_matches
```

Will give an example output such as:

```elixir
[
  %ThroneGDPR.MatchData{
    index: 123,
    user_id: "feedbeef",
    ...
  },
  ...
]
```

## Installation

Since the package is only available on GitLab, the package can be installed
by adding `throne_gdpr` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:throne_gdpr, git: "git@gitlab.com:Chaddo/throne_gdpr.git"}
  ]
end
```
